<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/app/Environment.php';
require __DIR__ . '/app/ShopifySync.php';

use App\ShopifySync;

$sync = new ShopifySync;
$sync->init();