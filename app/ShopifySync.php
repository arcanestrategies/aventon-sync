<?php

namespace App;

use Carbon\Carbon;
use ZfrShopify\ShopifyClient;
use ZfrTradeGecko\TradeGeckoClient;

class ShopifySync{
	private const SHOPIFY_API_VERSION = '2020-04';

	private $shopify;
	private $tradegecko;

	public function init(){
		$this->shopify = new ShopifyClient([
			'private_app' => true,
			'api_key' => Environment::get('SHOPIFY_API_KEY'),
			'password' => Environment::get('SHOPIFY_API_PASSWORD'),
			'shop' => Environment::get('SHOPIFY_URL'),
			'version' => ShopifySync::SHOPIFY_API_VERSION
		]);

		$this->tradegecko = new TradeGeckoClient(Environment::get('TRADE_GECKO_ACCESS_TOKEN'));
		$this->sync();
	}

	private function sync(){
		$purchase_orders = [];
		$purchase_order_line_item_ids = [];
		$variants = [];
		foreach($this->tradegecko->getPurchaseOrdersIterator(['status' => 'active']) as $purchase_order){
			$purchase_orders[$purchase_order['id']] = [
				'due_at' => $purchase_order['due_at'],
				'quantity' => intval($purchase_order['cached_quantity'])
			];
			$purchase_order_line_item_ids = array_merge($purchase_order_line_item_ids, $purchase_order['purchase_order_line_item_ids']);
		}
		
		foreach(array_chunk($purchase_order_line_item_ids, 100) as $purchase_order_line_item_ids_chunked){
			$line_items = $this->tradegecko->getPurchaseOrderLineItems(['ids' => $purchase_order_line_item_ids_chunked, 'limit' => 100, 'page' => 1]);
			foreach($line_items as $line_item){
				$variants[$line_item['variant_id']] = $purchase_orders[$line_item['purchase_order_id']];
			}
		}

		unset($purchase_orders);
		unset($purchase_order_line_item_ids);

		foreach($this->shopify->getOrdersIterator(['status' => 'open']) as $order){
			if($this->isPreOrder($order['tags'])){
				$items = $order['line_items'];
				foreach($items as $item){
					$variant = $item['variant_id'];
					$exists = array_key_exists($variant, $variants);
					if($exists){
						echo '';
					}
					echo '';
				}
			}
		}
		echo '';
	}

	private function isPreOrder($tags){
		if(empty($tags)){
			return false;
		}

		$tags = explode(',', $tags);
		foreach($tags as $tag){
			if(stristr($tag, 'preorder') || stristr($tag, 'pre-order')){
				return true;
			}
		}

		return false;
	}
}