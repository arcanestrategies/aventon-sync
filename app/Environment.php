<?php

namespace App;

use Symfony\Component\Dotenv\Dotenv;

class Environment{
	private static $ENV_LOADED = false;

	public static function get($key){
		if(!Environment::$ENV_LOADED){
			Environment::loadEnvironment();
		}

		if(array_key_exists($key, $_ENV)){
			return Environment::parse($_ENV[$key]);
		}

		return null;
	}

	private static function loadEnvironment(){
		$dotenv = new Dotenv();
		$dotenv->load(dirname(__DIR__) . '/.env');
		Environment::$ENV_LOADED = true;
	}

	private static function parse($var){
		if(is_numeric($var)){
			if(stristr($var, '.')){
				return doubleval($var);
			}
			return intval($var);
		}

		if(strcmp($var, 'true') === 0 || strcmp($var, 'false') === 0){
			return strcmp($var, 'true') === 0 ? true : false;
		}

		return $var;
	}
}