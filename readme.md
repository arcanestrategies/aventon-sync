# Setup

Clone the repository to your preferred location, then run `composer install` to install the project requirements.
Afterwards create a .env file in the project's root directory, using the .env.example file for reference and finally fill out the variables.

# Run

Simply run `php sync.php` from within the project's root directory.